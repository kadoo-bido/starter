
var gulp 			= require('gulp'),
	sass 			= require('gulp-ruby-sass'),
	autoprefixer 	= require('gulp-autoprefixer'),
	minifycss 		= require('gulp-minify-css'),
	jshint 			= require('gulp-jshint'),
	uglify 			= require('gulp-uglify'),
	imagemin 		= require('gulp-imagemin'),
	rename 			= require('gulp-rename'),
	concat 			= require('gulp-concat'),
	notify 			= require('gulp-notify'),
	cache 			= require('gulp-cache'),
	livereload 		= require('gulp-livereload'),
	del 			= require('del'),
	mainBowerFiles 	= require('main-bower-files'),
	inject 			= require('gulp-inject');



gulp.task("bower-files", function(){
    return gulp.src(mainBowerFiles() , {base: './bower_components'})
        .pipe(gulp.dest("./dist/assets/libs"));
});


gulp.task('styles', function() {
	return sass('src/css/main.scss', { style: 'expanded' })
		.pipe(autoprefixer('last 2 version'))
		.pipe(gulp.dest('dist/assets/css'))
		.pipe(rename({suffix: '.min'}))
		.pipe(minifycss())
		.pipe(gulp.dest('dist/assets/css'))
		.pipe(notify({ message: 'Styles task complete' }));
});



gulp.task('scripts', function() {
	return gulp.src('src/js/**/*.js')
		.pipe(jshint('.jshintrc'))
		.pipe(jshint.reporter('default'))
		.pipe(concat('app.js'))
		.pipe(gulp.dest('dist/assets/js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(gulp.dest('dist/assets/js'))
		.pipe(notify({ message: 'Scripts task complete' }));
});



gulp.task('index', function () {
	var target = gulp.src('dist/index.html');
	// It's not necessary to read the files (will speed up things), we're only after their paths: 
	var sources = gulp.src(['./dist/assets/libs/**/*.js',  './dist/assets/libs/**/*.css'], {read: false });

	return  target.pipe( inject(sources , {relative: true}) )
			.pipe(gulp.dest('./dist/'));
});


gulp.task('images', function() {
	return gulp.src('src/images/**/*')
		.pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
		.pipe(gulp.dest('dist/assets/img'))
		.pipe(notify({ message: 'Images task complete' }));
});


gulp.task('watch', function() {

	// Watch .scss files
	gulp.watch('src/css/**/*.scss', ['styles']);

	// Watch .js files
	gulp.watch('src/js/**/*.js', ['scripts']);

	// Watch image files
	gulp.watch('src/images/**/*', ['images']);

});


gulp.task('watch', function() {

  // Create LiveReload server
  livereload.listen();

  // Watch any files in dist/, reload on change
  gulp.watch(['dist/**']).on('change', livereload.changed);

});



gulp.task('default', ['bower-files' , 'styles' , 'scripts']);